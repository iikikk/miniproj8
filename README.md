# Mini Project 8

## Project description:

This Rust command-line tool is designed to efficiently process input documents, specifically to count the number of negative numbers within them. It showcases fundamental Rust programming techniques including data ingestion from files, command-line argument parsing, and robust error handling. The project is structured to include comprehensive unit tests, ensuring reliability and correctness in its functionality. Aimed at demonstrating practical Rust skills, this tool combines basic file I/O operations, data processing, and software testing methodologies in a cohesive application.

## Requirement

-	Rust command-line tool
-	Data ingestion/processing
-	Unit tests

## Steps

1. Initialize Rust Project
Create a new Rust project. 
cargo new mini8

2. Add necessary dependencies to Cargo.toml file.
[dependencies]
clap = "3.0"

3. Prepare the test.txt under the project directory for testing.

4. Build the project:
cargo build –release

5. Use the following command to run the tool:
cargo run -- -i test.txt

6. Run unit tests with:
cargo test


## Screenshot:

### Build:
 ![Alt text](image.png)

### Unit test:
 ![Alt text](image-1.png)

### Sample Output:
![Alt text](image-2.png)